import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/CustomerList').default,
        name:'customer-list',
        meta: {
            title: 'List',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
];


// This callback runs before every route change, including on page load.


const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});

export default router;
