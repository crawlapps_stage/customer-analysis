<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify', 'billable'])->name('home');

Route::group(['middleware' => ['auth.shopify', 'billable']], function () {
    Route::resource('customer', 'Customer\CustomerController');
});

Route::get('flush', function(){
    request()->session()->flush();
});
//
//Auth::routes();

