<?php namespace App\Jobs;

use App\Models\Customer;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use stdClass;

class CustomersUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("======================== Customer update webhook =======================");
        $shopDomain = $this->shopDomain->toNative();
        $user = User::select('id')->where('name', $shopDomain)->first();
        $user_id = ( $user ) ? $user->id : '';
        $sh_customer = $this->data;

        \Log::info(json_encode($sh_customer));
        $customer = Customer::where('customer_id', $sh_customer->id)->where('user_id', $user_id)->first();

        $name = ( $sh_customer->first_name ) ? $sh_customer->first_name : '';
        $name = ( $sh_customer->last_name ) ? $name . ' ' . $sh_customer->last_name : $name;

        $customer = ( $customer ) ? $customer : new Customer;
        $customer->user_id = $user_id;
        $customer->customer_id = $sh_customer->id;
        $customer->name = $name;
        $customer->email = $sh_customer->email;
        $customer->last_updated_at = date("Y-m-d H:i:s", strtotime($sh_customer->updated_at));
        $customer->save();
    }
}
