<?php

namespace App\Jobs;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("======================== After Authentication Job =======================");
        $shop = \Auth::user();

        $endPoint = '/admin/api/'. env('SHOPIFY_API_VERSION') .'/shop.json';
        $parameter['fields'] = 'id,email,address1,city,province,zip,currency,country';
        $result = $shop->api()->rest('GET', $endPoint);
        if( !$result['errors'] ){
            $sh_shop = $result['body']->container['shop'];
            $address = $sh_shop['address1'] . ' ' . $sh_shop['city'] . ' ' .  $sh_shop['zip'] . ' ' . $sh_shop['province'] . ' ' . $sh_shop['country'];
            $shop->email = $sh_shop['email'];
            $shop->currency = $sh_shop['currency'];
            $shop->address = $address;
            $shop->save();
        }
        $endPoint = '/admin/api/'. env('SHOPIFY_API_VERSION') .'/customers/count.json';
        $result = $shop->api()->rest('GET', $endPoint);
        if( !$result['errors'] ){
            $count = $result['body']->container['count'];
            $total_page = number_format(ceil($count / 250), 0);

            for ($i = 1; $i <= $total_page; $i++) {
                $parameter['limit'] = 250;
                $parameter['page'] = $i;
                $parameter['fields'] = 'id,first_name,last_name,email,updated_at';
                $result = $shop->api()->rest('GET', 'admin/customers.json', $parameter);
                if( !$result['errors'] ){
                    $customers = $result['body']->container['customers'];
                    if( is_array($customers) && !empty($customers)){
                        foreach( $customers as $key=>$val ){
                            $name = ( $val['first_name'] ) ? $val['first_name'] : '';
                            $name = ( $val['last_name'] ) ? $name . ' ' . $val['last_name'] : $name;
                            $customer = new Customer;
                            $customer->user_id = $shop->id;
                            $customer->customer_id = $val['id'];
                            $customer->name = $name;
                            $customer->email = $val['email'];
                            $customer->last_updated_at = date("Y-m-d H:i:s", strtotime($val['updated_at']));
                            $customer->save();
                        }
                    }
                }
            }
        }
    }
}
