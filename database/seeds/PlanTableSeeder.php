<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('plans')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');

        \DB::table('plans')->insert(
            [
                [
                    'type' => "RECURRING",
                    'name' => 'Basic Plan',
                    'price' => 1.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Basic Plan',
                    'trial_days' => 0,
                    'test' => 1,
                    'on_install' => 1
                ],
            ]
        );
    }
}
